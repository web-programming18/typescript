class Person {
    public constructor(private name: string) {
    }
  
    public getName(): string {
      return this.name;
    }
  }
  
  const person = new Person("Jane");
  console.log(person);
  console.log(person.getName());


  interface Shape {
    getArea: () => number;
  }
  
  class Rectangle1 implements Shape {
    public constructor(protected readonly width1: number, protected readonly height1: number) {}
  
    public getArea(): number {
      return this.width1 * this.height1;
    }

  }
  const rest:Rectangle1 = new Rectangle1(50,10);
  console.log(rest);
  console.log(rest.getArea());

  class Square extends Rectangle1 {
    public constructor(width: number) {
      super(width, width);
    }
    public toString(): string {
        return `Square [${this.width1}]`;
      }
}

const sq = new Square(10);
console.log(sq);
console.log(sq.getArea());


abstract class Polygon {
    public abstract getArea(): number;
  
    public toString(): string {
      return `Polygon[area=${this.getArea()}]`;
    }
  }
  
  class Rectangle extends Polygon {
    public constructor(protected readonly width1: number, protected readonly height1: number) {
      super();
    }
  
    public getArea(): number {
      return this.width * this.height;
    }
  }